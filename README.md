# docker-kit
Docker container for quickly getting up and running with
[Kit](https://www.kitlang.org/)

## Usage
Create a docker-compose.yml like this in your project:
```yaml
version: '3'
services:
  kit:
build: .
  volumes:
    - .:/code
```

Create a Makefile like this:
```Makefile
run:
	kitc hello --run
```

The Docker container will run `make` when it starts.
You can also refer to the samples in this repository.
