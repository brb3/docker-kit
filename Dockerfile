FROM 'haskell:8.4.3'

RUN git clone --depth 1 https://github.com/kitlang/kit.git
RUN cd kit ; \
    stack build ; \
    stack test ; \
    stack install

ENV KIT_STD_PATH=/kit/std
ENTRYPOINT cd /code ; make
